# digitaltolk-test

X. A readme with:   Your thoughts about the code. What makes it amazing code. Or what makes it ok code. Or what makes it terrible code. How would you have done it. Thoughts on formatting, structure, logic.. The more details that you can provide about the code (what's terrible about it or/and what is good about it) the easier for us to assess your coding style, mentality etc

 - The code maybe working but could still use some improvements. I could only do some refactoring because I am not familiar the functionalities and at some point I was kind of getting confused about the use of the job naming convention: is it a laravel queue or a controller for the user job?

- Some variable initializations could be shortened (ternary operations), as well as some conditions. But some need not changed for readability. 
- Some variables can also be put in a config file so that we can add more values in the future dynamically. Here are some example:

$certifications = [
	'normal' => 'normal',
	'certified' => 'yes',
	'certified_in_law' => 'law',
	'certified_in_health' => 'health'
];

$gender = ['male' => 'Man', 'female' => 'Kvinna'];
$usertype = ['customer', 'translator'];


- Mailer seems to be performed directly, not sure if we have separate service/jobs for that, if not we can add one so that server will never slow down
- 